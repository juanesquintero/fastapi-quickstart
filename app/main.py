import uvicorn
from app.routers import courses_router
from fastapi import FastAPI

app = FastAPI()

@app.get('/', tags=['Root'])
async def root() -> dict:
    return {'api': 'FastAPI Quickstart'}

app.include_router(courses_router)

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
