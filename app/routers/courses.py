from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db.database import get_db
from app.models import courses as model
from app.schemas.courses import CourseSchema, CourseMsg, CourseInsertion, List

courses_router = APIRouter()
tag = dict(tags=['Courses'])

@courses_router.get('/courses', **tag)
@courses_router.get('/courses/', **tag)
def get_courses(db: Session = Depends(get_db)) -> List[CourseSchema]:
    return model.read_courses(db)

@courses_router.get('/courses/{id}', **tag)
def get_course(id: str, db: Session = Depends(get_db)) -> CourseSchema:
    return model.read_course(db, id)

@courses_router.get('/courses/slice/fragment', **tag)
def get_courses_slice(
        start: int,
        end: int,
        db: Session = Depends(get_db)
    ) -> List[CourseSchema]:
    return model.read_courses_slice(db, start, end)

@courses_router.post('/courses', **tag)
def post_course(
        course: CourseInsertion,
        db: Session = Depends(get_db)
    ) -> CourseMsg:
    return model.create_course(db, course)

@courses_router.delete('/courses/{id}', **tag)
def delete_course(id: str, db: Session = Depends(get_db)) -> CourseMsg:
    return model.delete_course(db, id)

@courses_router.put('/courses/{id}', **tag)
def put_course(
        id: str,
        course: CourseInsertion,
        db: Session = Depends(get_db)
    ) -> CourseMsg:
    return model.update_course(db, id, course)
